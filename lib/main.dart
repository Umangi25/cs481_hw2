import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_hw2/constants.dart';

void main() => runApp(MyApp());
class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      title: 'Welcome to Travel Info App',
      home: Scaffold(
        backgroundColor: Color(0xff000000),appBar: AppBar(

          title:
          Text('Favourite Place', textAlign: TextAlign.start,style: TextStyle(fontSize: 30, color: Colors.black ,fontFamily: 'DancingScript1')
    ),

    actions: [
    Container(
      child: Center(
        child: Padding(
          padding: const EdgeInsets.only(right: 130),
          child: Icon(Icons.favorite,
          color: Colors.black,
          ),
        ),
      ),
    )
    ],
    ),
    body: Column(
      children: <Widget>[
      Row(

        children: <Widget>[

          Text('''Morning View    ''',textAlign: TextAlign.center, style: TextStyle(fontSize: 25, color: Colors.teal, fontFamily: 'DancingScript'),
           ),
          new Image.asset('assests/beach.jpeg',
            width: 200,
            height: 150,
          ),

        ],
      ),
        Row(
          
          children: <Widget>[
            new Image.asset('assests/beach2.jpeg',
              width: 200,
              height: 150,
            ),
          Text('    Evening View',textAlign: TextAlign.center, style: TextStyle(fontSize: 25, color: Colors.teal ,fontFamily: 'DancingScript'),)
        ],
        ),
        Container(
          color: Colors.blue,
          child: Row(
              children: <Widget>[
                Text("  Blogs on Beaches   ",textAlign: TextAlign.start,style: TextStyle(fontSize: 30, color: Colors.black ,fontFamily: 'DancingScript1'),),
                Icon(Icons.beach_access, ),
              ]
          ),
        ),
        Column(
              children:[
                Align(
                  alignment: Alignment.center,
                  child:Text('One of my most favorite places is the beach.'
                    ' I love the water and the sand. The sun seems the always be shining.'
                      ' The air smells like salt and sand. The sky looks so pretty over the ocean especially during sunrise and sunsets.'
                    'I like when it is not as crowed because it seems clam. People there seem clam and not so stressed.'
                    'People take their time to enjoy there surrounds and take all the sights in.'
                    'Overtime, humans have changed the beach, along with other places,'
                    'by adding more sand or building things all along the shore to attack tourists.',textAlign: TextAlign.justify,
                    style:TextStyle(
                      fontFamily: 'DancingScript',
                      color: Colors.white,
                      fontSize: 19,
                    ),
                  ),
                ),
              ]
          ),

        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: const <Widget>[
            Icon(
              Icons.favorite,
              color: Colors.pink,
              size: 24.0,
              semanticLabel: 'Text to announce in accessibility modes',
            ),
            Icon(
              Icons.audiotrack,
              color: Colors.green,
              size: 24.0,
            ),
            Icon(
              Icons.beach_access,
              color: Colors.blue,
              size: 24.0,
            ),
          ],
        )
    ],
        )
    )
      );
  }
}
